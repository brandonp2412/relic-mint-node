import fs from "fs";
import Items from "warframe-items";
import { getMedian } from "./get-median";
import OrderModel from "./order.model";
import { getRelicOrders } from "./warframe";

function timeout(ms: number, promise: Promise<any>): Promise<any> {
  return new Promise((resolve, reject) => {
    const timer = setTimeout(() => {
      reject(new Error("TIMEOUT"));
    }, ms);

    promise
      .then((value) => {
        clearTimeout(timer);
        resolve(value);
      })
      .catch((reason) => {
        clearTimeout(timer);
        reject(reason);
      });
  });
}

const main = async () => {
  const relics = Array.from(new Items({ category: ["Relics"] }))
    .map((item) => item.name.replace(/ \w+$/, ""))
    .reduce<string[]>(
      (unique, item) => (unique.includes(item) ? unique : [...unique, item]),
      []
    );
  const relicValues: { name: string; value: number }[] = [];
  for (let i = 0; i < relics.length; i++) {
    console.log(`Getting relic: ${relics[i]}\t\t(${i} / ${relics.length})`);
    let response: { payload: { orders: OrderModel[] } };
    try {
      response = await timeout(3000, getRelicOrders(relics[i]));
    } catch {
      console.error(`Failed to get relic: ${relics[i]}`);
      continue;
    }
    if (!response.payload) {
      console.error(response);
      continue;
    }
    const sells = response.payload.orders
      .map((order) => order.platinum)
      .sort((a, b) => a - b);
    relicValues.push({
      name: relics[i],
      value: getMedian(sells),
    });
  }
  fs.writeFileSync(
    "./data/relics.json",
    "Relic,Value\n" +
      relicValues.map((relic) => `${relic.name},${relic.value || 0}\n`).join("")
  );
};

main();

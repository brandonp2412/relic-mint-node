import { User } from "./user.model";

export default interface OrderModel {
  quantity: number;
  platinum: number;
  order_type: string;
  user: User;
  platform: string;
  region: string;
  creation_date: Date;
  last_update: Date;
  visible: boolean;
  subtype: string;
  id: string;
}

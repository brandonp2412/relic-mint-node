import OrderModel from "./order.model";
import fetch from "node-fetch";

const BASE_URL = "https://api.warframe.market/v1";

export const getRelicOrders = async (
  relic: string
): Promise<{ payload: { orders: OrderModel[] } }> => {
  const snakeRelic = relic.toLowerCase().replace(/ /g, "_");
  const response = await fetch(`${BASE_URL}/items/${snakeRelic}_relic/orders`);
  return response.json();
};

export const getItemOrders = async (
  item: string
): Promise<{ payload: { orders: OrderModel[] } }> => {
  const snakeItem = item.toLowerCase().replace(/ /g, "_");
  const response = await fetch(`${BASE_URL}/items/${snakeItem}/orders`);
  return response.json();
};

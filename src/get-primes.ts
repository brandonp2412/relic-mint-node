import fs from "fs";
import { getItemOrders } from "./warframe";

const main = async () => {
  let primes: string[] = JSON.parse(
    fs.readFileSync("./primes.json").toString()
  );
  const parts = [
    "Chassis",
    "Neuroptics",
    "Systems",
    "Collar",
    "Harness",
    "Wings",
  ];
  primes = primes.map((prime) => {
    for (const part of parts) {
      if (prime.includes(`${part} Blueprint`))
        return prime.replace(`${part} Blueprint`, part);
    }
    return prime;
  });
  const itemValues: { name: string; value: number }[] = [];
  for (let i = 0; i < primes.length; i++) {
    console.log(`Getting item ${primes[i]} (${i}/${primes.length - 1})`);
    let response;
    try {
      response = await getItemOrders(primes[i]);
    } catch (error) {
      console.error(error);
    }
    if (!response?.payload) {
      console.error(response);
      continue;
    }
    const prices = response.payload.orders
      .filter(
        (order) => order.order_type === "sell" && order.user.status === "ingame"
      )
      .map((order) => order.platinum)
      .sort((a, b) => a - b);
    itemValues.push({
      name: primes[i],
      value: prices[0],
    });
  }
  fs.writeFileSync(
    "./data/primes.csv",
    "Prime,Value\n" +
      itemValues
        .map((itemValue) => `${itemValue.name},${itemValue.value}\n`)
        .join("")
  );
};

main();

export interface User {
  ingame_name: string;
  last_seen: Date;
  reputation: number;
  region: string;
  status: string;
  id: string;
  avatar?: any;
}

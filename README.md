# Installation

This project requires [Node.js](https://nodejs.org).

```
npm install
```

# Building

```
npx tsc
```

# Getting Relics

```
node dist/get-relics.js
```

Then relic values can be found in `data/relics.csv`.

# Getting prime part values

```
node dist/get-primes.js
```

Then prime part values can be found in `data/primes.csv`.
